<x-backend.layouts.master>

    <x-slot name="pageTitle">
        Details
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">Sizes</x-slot>
            <li class="breadcrumb-item"><a href="{{ 'home' }}">Dashboard</a></li>
            {{-- <li class="breadcrumb-item"><a href="{{ 'table' }}">Tables</a></li> --}}
            <li class="breadcrumb-item active">Add New</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
    {{-- <div class="container"> --}}
        
        {{-- <div class="row justify-content-center"> --}}
            
            {{-- <div class="col-lg-7"> --}}
                {{-- <div class="card shadow-lg border-0 rounded-lg mt-5"> --}}
                    <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Size Details<a class="bt btn-sm btn-info" href="{{ route('sizes.index') }}">List</a>
                    </div>
                    {{-- <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Create Category</h3>
                    </div> --}}
                    <div class="card-body">
                        <h3>Title:{{ $size->title }}</h3>
                        <p>Description:{{ $size->description }}</p>
                    
                    </div>

                </div>
            {{-- </div> --}}
        {{-- </div> --}}
    {{-- </div> --}}
</x-backend.layouts.master>