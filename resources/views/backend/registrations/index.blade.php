<x-backend.layouts.master>

    <x-slot name="pageTitle">
        Sizes
    </x-slot>
    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">Sizes</x-slot>
            <li class="breadcrumb-item"><a href="{{ 'home' }}">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="{{'registrations'}}">Add New</a></li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
   

    

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table me-1"></i>
        Sizes<a class="bt btn-sm btn-info" href="{{ route('registrations.create') }}">Add new</a>
    </div>
    <div class="card-body">
        @if (session('message'))
        <div class="alert alert-success">
            <span class="close" data-dismiss="alert">&times;
            </span>
            <strong>{{ session ('message')}}.</strong>
    </div>
    @endif

        <table id="datatablesSimple">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Present Address</th>
                    <th>Permanent Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Zip Code</th>
                    <th>Gender</th>
                    <th>Action<th>
                   
                </tr>
            </thead>
            
            <tbody>
                @php
                    $sl=0
                @endphp
                @foreach ( $registrations as $registration )

                <tr>
                    <td>{{ ++$sl }}</td>
                    <td>{{ $registration->firstname }}</td>
                    <td>{{ $registration->lastname }}</td>
                    <td>{{ $registration->email }}</td>
                    <td>{{ $registration->password }}</td>
                    <td>{{ $registration->presentAddress }}</td>
                    <td>{{ $registration->permanentAddress }}</td>
                    <td>{{ $registration->city }}</td>
                    <td>{{ $registration->state }}</td>
                    <td>{{ $registration->zipcode }}</td>
                    <td>{{ $registration->gender }}</td>
                    
                    
                    <td>
                        <a class="btn btn-sm btn-info" href="{{ route('registrations.show',['registration'=>$registration->id]) }}" >Show</a>
                        <a class="btn btn-sm btn-warning" href="{{ route('registrations.edit',['registration'=>$registration->id]) }}" >Edit</a>
                        <form style="display: inline" action="{{ route('registrations.destroy',['registration'=>$registration->id]) }}" method="post">
                        @csrf
                        @method('delete')
                        <button onclick="return confirm('Are you sure want to delete?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                        </form>
                        {{-- <a href="{{ route('categories.destroy',['category'=>$category->id]) }}" >Delete</a> --}}

                    </td>
                    
                </tr>
                    
                @endforeach
                
                
            </tbody>
        </table>
    </div>
</div>

    
    </x-backend.layouts.master>
    


