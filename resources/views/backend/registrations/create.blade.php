<x-backend.layouts.master>

    <x-slot name="pageTitle">
        Registration
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">Registrations</x-slot>
            <li class="breadcrumb-item"><a href="{{ 'home' }}">Dashboard</a></li>
            {{-- <li class="breadcrumb-item"><a href="{{ 'table' }}">Tables</a></li> --}}
            {{-- <li class="breadcrumb-item active">Add New</li> --}}
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card mb-4">
        <div class="card-header ">
            
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Create</span>
                <span>
                    <a class="btn btn-primary text-left" href="#" role="button">List</a>
                </span>
            </div>
            @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}</p>
            @endif
        </div>
        <div class="card-body">
            <form  action="{{ route('registrations.store') }}" method="post">
                @csrf
                <div class="row mb-3">
                <div class="col-md-6 ">
                    <label for="inputFirstName" class="form-label">First Name</label>
                    <input type="text" name="firstname" class="form-control" id="inputFirstName" placeholder="First Name" value="{{ old('firstname') }}" >
                    @error('firstname')
                                <span class="sm text-danger">{{ $message }}</span>
                            @enderror
                </div>
                <div class="col-md-6 ">
                  <label for="inputLastName" class="form-label">Last Name</label>
                  <input type="text" name="lastname" class="form-control" id="inputLastName" placeholder="Last Name" value="{{ old('lastname') }}">
                  @error('lastname')
                                <span class="sm text-danger">{{ $message }}</span>
                            @enderror
                </div>
                </div>
                <div class="row mb-3">
                <div class="col-md-6 ">
                  <label for="inputEmail" class="form-label">Email</label>
                  <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ old('email') }}">
                  @error('email')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
                </div>
                <div class="col-md-6">
                  <label for="inputPassword" class="form-label">Password</label>
                  <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password" value="{{ old('password') }}">
                  @error('password')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
                </div>
                </div>
        
              <div class="row mb-3">
              <div class="col-md-12 mb-3">
                <label for="inputpresentAddress" class="form-label">Present Address</label>
                <input type="text" name="presentAddress" class="form-control" id="inputAddress1" placeholder="Present Address" value="{{ old('presentAddress') }}">
                @error('presentAddress')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
              </div>
              <div class="col-md-12">
                <label for="inputAddress2" class="form-label">Permanent Address</label>
                <input type="text" name="permanentAddress" class="form-control" id="inputAddress2" placeholder="Permanent Address" value="{{ old('permanentAddress') }}">
                @error('permanentAddress')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
              </div>
              </div>
        
              <div class="row mb-3">
              <div class="col-md-6">
                <label for="inputCity" class="form-label">City</label>
                <input type="text" name="city" class="form-control" id="inputCity" value="{{ old('city') }}">
                @error('city')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
              </div>
              <div class="col-md-4">
                <label for="inputState" class="form-label">State</label><br>
                <select name="state"  class="form-select" style="height: 38px; border:1px solid 	#DCDCDC; border-radius:5px; width:100%;" aria-label="Default select example">
                  <option value="">Choose... </option>
                  <option value="Dhaka" {{ old('state') == 'Dhaka' ? 'selected': '' }}>Dhaka</option>
                  <option value="Barisal" {{ old('state') == 'Barisal' ? 'selected': '' }}>Barisal</option>
                  <option value="Sylhet" {{ old('state') == 'Sylhet' ? 'selected': '' }}>Sylhet</option>
                  @error('state')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
                </select>
              </div>
              <div class="col-md-2">
                <label for="inputZip" class="form-label">Zip Code</label>
                <input type="text" name="zipcode" class="form-control" id="inputZip" value="{{ old('zipcode') }}">
                @error('zipcode')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
              </div>
              </div>
        
              <div class="row mb-3">
              <div class="col-md-12">
                Gender :
                    
                      <input type="radio" value="Male" id="male" name="gender" {{ old('gender')=='Male'?'checked':'' }}>
                      <label for="male"> Male</label>
              
                      <input type="radio" id="female"   value="Female" name="gender" {{ old('gender')=='Female'?'checked':''}}>
                      <label for="female"> Female</label>
                    
                      <input type="radio" id="other"  value="Other" name="gender" {{ old('gender')=='Other'?'checked':''}}>
                      <label for="other"> Other</label>
                      @error('gender')
                    <span class="sm text-danger">{{ $message }}</span>
                    @enderror
              </div>
              </div>
              <div class="row mb-3">
              <div class="col-md-12">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="gridCheck" name="remember">
                  <label class="form-check-label" for="gridCheck">
                  Agree
                  </label>
                  @error('remember')
                    <span class="sm text-danger">{{ $message }}</span>
                @enderror
                </div>
              </div>
              </div>
        
              <div class="row">
              <div class="col-md-12">
                <a href="login.php" class="btn btn-primary ">Login</a>
                <button type="submit" name="sign_up" class="btn btn-primary right-btn">Submit</button>
        
              </div>
              </div>
              
              
            </form>
        </div>
    </div>

  
</x-backend.layouts.master>