<x-backend.layouts.master>

    <x-slot name="pageTitle">
        Edit Form
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">Sizes</x-slot>
            <li class="breadcrumb-item"><a href="{{ 'home' }}">Dashboard</a></li>
            {{-- <li class="breadcrumb-item"><a href="{{ 'table' }}">Tables</a></li> --}}
            <li class="breadcrumb-item active">Add New</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
    {{-- <div class="container"> --}}
        
        {{-- <div class="row justify-content-center"> --}}
            
            {{-- <div class="col-lg-7"> --}}
                {{-- <div class="card shadow-lg border-0 rounded-lg mt-5"> --}}
                    <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Edit Registration<a class="bt btn-sm btn-info" href="{{ route('registrations.index') }}">List</a>
                    </div>
                    
                    <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                        <form action="{{ route('registrations.update',['registration'=>$registration->id]) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="form-floating mb-3 mb-md-0">
                                <input name="firstname" class="form-control" id="inputFirstName" 
                                type="text" placeholder="Enter your title"
                                value="{{old('firstname',$registration->firstname)}}" />
                                <label for="inputFirstName">First Name</label>

                                @error('firstname')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="lastname" class="form-control" id="inputLastName" 
                                type="text" placeholder="Enter your title"
                                value="{{old('lastname',$registration->lastname)}}" />
                                <label for="inputLastName">Last Name</label>

                                @error('lastname')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="email" class="form-control" id="inputEmail" 
                                type="email" placeholder="Enter your email"
                                value="{{old('email',$registration->email)}}" />
                                <label for="inputEmail">Email</label>

                                @error('email')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="password" class="form-control" id="inputPassword" 
                                type="password" placeholder="Enter your title"
                                value="{{old('password',$registration->password)}}" />
                                <label for="inputPassword">Password</label>

                                @error('password')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="PresentAddress" class="form-control" id="inputPresentAddress" 
                                type="text" placeholder="Enter your title"
                                value="{{old('presentAddress',$registration->presentAddress)}}" />
                                <label for="inputPresentAddress">Present Address</label>

                                @error('presentAddress')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="PermanentAddress" class="form-control" id="inputPermanentAddress" 
                                type="text" placeholder="Enter your title"
                                value="{{old('permanentAddress',$registration->permanentAddress)}}" />
                                <label for="inputPermanentAddress">Permanent Address</label>

                                @error('permanentAddress')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="city" class="form-control" id="inputCity" 
                                type="text" placeholder="Enter your city"
                                value="{{old('city',$registration->city)}}" />
                                <label for="inputCity">City</label>

                                @error('city')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="state" class="form-control" id="inputState" 
                                type="text" placeholder="Enter your state"
                                value="{{old('state',$registration->state)}}" />
                                <label for="inputState">State</label>

                                @error('state')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="zipcode" class="form-control" id="inputZipcode" 
                                type="text" placeholder="Enter your state"
                                value="{{old('zipcode',$registration->zipcode)}}" />
                                <label for="inputZipcode">Zip Code</label>

                                @error('zipcode')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mb-3 mb-md-0">
                                <input name="gender" class="form-control" id="inputState" 
                                type="text" placeholder="Enter your state"
                                value="{{old('gender',$registration->gender)}}" />
                                <label for="inputState">Gender</label>

                                @error('gender')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            {{-- <div class="form-floating mt-3">
                                <textarea name="description" class="form-control" id="inputDescription" 
                                placeholder="description">
                                {{old('description',$size->description)}}
                                </textarea>
                                <label for="inputDescription">Description</label>

                                @error('description')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div> --}}

                            {{-- <div class="form-floating mt-3">
                                <input name="image" type="file" class="form-control" id="inputImage">
                                
                                <label for="inputImage">Image</label>
                            </div> --}}
                            <div class="mt-4 mb-0">
                                <button type="submit" class="btn btn-primary" >Save</button>
                            </div>

                        </form>
                    </div>
                    {{-- <div class="card-footer text-center py-3">
                        <div class="small"><a href="{{'login'}}">Have an account? Go to login</a></div>
                    </div> --}}
                </div>
            {{-- </div> --}}
        {{-- </div> --}}
    {{-- </div> --}}
</x-backend.layouts.master>