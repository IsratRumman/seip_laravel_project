<x-backend.layouts.master>

    <x-slot name="pageTitle">
        Edit Form
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">Categories</x-slot>
            <li class="breadcrumb-item"><a href="{{ 'home' }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ 'table' }}">Tables</a></li>
            <li class="breadcrumb-item active">Add New</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
    {{-- <div class="container"> --}}
        
        {{-- <div class="row justify-content-center"> --}}
            
            {{-- <div class="col-lg-7"> --}}
                {{-- <div class="card shadow-lg border-0 rounded-lg mt-5"> --}}
                    <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Edit Category<a class="bt btn-sm btn-info" href="{{ route('categories.index') }}">List</a>
                    </div>
                    {{-- <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Create Category</h3>
                    </div> --}}
                    <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                        <form action="{{ route('categories.update',['category'=>$category->id]) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="form-floating mb-3 mb-md-0">
                                <input name="title" class="form-control" id="inputTitle" 
                                type="text" placeholder="Enter your title"
                                value="{{old('title',$category->title)}}" />
                                <label for="inputFirstName">Title</label>

                                @error('title')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>
                            <div class="form-floating mt-3">
                                <textarea name="description" class="form-control" id="inputDescription" 
                                placeholder="description" value="{{old('description',$category->description)}}">
                                </textarea>
                                <label for="inputDescription">Description</label>

                                @error('description')
                                  <span class="small text-danger">{{ $message }}</span>  
                                @enderror
                            </div>

                            <div class="form-floating mt-3">
                                <input name="image" type="file" class="form-control" id="inputImage">
                                
                                <label for="inputImage">Image</label>
                            </div>
                            <div class="mt-4 mb-0">
                                <button type="submit" class="btn btn-primary" >Save</button>
                            </div>

                        </form>
                    </div>
                    <div class="card-footer text-center py-3">
                        <div class="small"><a href="{{'login'}}">Have an account? Go to login</a></div>
                    </div>
                </div>
            {{-- </div> --}}
        {{-- </div> --}}
    {{-- </div> --}}
</x-backend.layouts.master>