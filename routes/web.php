<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function(){

    return view('welcome');
});
// Route::get('/home', function(){

//     return view('backend.home');
// });

Route::get('/home', function(){

    return view('backend/home');
});

Route::get('/categories',[CategoryController::class,'index'])->name('categories.index');

Route::get('/categories/create', [CategoryController::class,'create'])->name('categories.create');

Route::post('/categories', [CategoryController::class,'store'])->name('categories.store');

Route::get('/categories{category}', [CategoryController::class,'show'])->name('categories.show');
// GET	/photos/{photo}/edit	edit	photos.edit

Route::get('/categories{category}/edit', [CategoryController::class,'edit'])->name('categories.edit');

// PUT/PATCH	/photos/{photo}	update	photos.update
Route::patch('/categories{category}', [CategoryController::class,'edit'])->name('categories.update');

// DELETE	/photos/{photo}	destroy	photos.destroy

Route::delete('/categories{category}', [CategoryController::class,'destroy'])->name('categories.destroy');


// SizeRoute..........


Route::get('/sizes', [SizeController::class,'index'])->name('sizes.index');
Route::get('/sizes/create', [SizeController::class,'create'])->name('sizes.create');
Route::post('/sizes', [SizeController::class,'store'])->name('sizes.store');

Route::get('/sizes{size}', [SizeController::class,'show'])->name('sizes.show');
// GET	/photos/{photo}/edit	edit	photos.edit

Route::get('/sizes{size}/edit', [SizeController::class,'edit'])->name('sizes.edit');

// PUT/PATCH	/photos/{photo}	update	photos.update
Route::patch('/sizes{size}', [SizeController::class,'update'])->name('sizes.update');

// DELETE	/photos/{photo}	destroy	photos.destroy

Route::delete('/sizes{size}', [SizeController::class,'destroy'])->name('sizes.destroy');



Route::get('/login', function(){

    return view('backend/login');

})->name('login');
// Route::get('create', function(){

//     return view('backend/registrations/create');

// })->name('create');
//Registration Link.....
Route::get('/registrations', [RegistrationController::class,'index'])->name('registrations.index');
Route::get('/Registrations/create', [RegistrationController::class,'create'])->name('registrations.create');
Route::post('/Registrations', [RegistrationController::class,'store'])->name('registrations.store');

Route::get('/registrations{registration}', [RegistrationController::class,'show'])->name('registrations.show');
// GET	/photos/{photo}/edit	edit	photos.edit

Route::get('/registrations{registration}/edit', [RegistrationController::class,'edit'])->name('registrations.edit');

// PUT/PATCH	/photos/{photo}	update	photos.update
Route::patch('/registrations{registration}', [RegistrationController::class,'update'])->name('registrations.update');

// DELETE	/photos/{photo}	destroy	photos.destroy

Route::delete('/registrations{registration}', [RegistrationController::class,'destroy'])->name('registrations.destroy');



