<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Size::create([
            'title'=>'Extra Large',
            'description'=>'Chest 124cm,Back 90 cm, waist 112 cm, arm 73 cm, collar 43-44',
        ]);
        \App\Models\Size::create([
            'title'=>'Large',
            'description'=>'Chest 116cm,Back 89 cm, waist 104 cm, arm 73 cm, collar 41-42',
        ]);
        \App\Models\Size::create([
            'title'=>'Medium',
            'description'=>'Chest 110cm,Back 88 cm, waist 98 cm, arm 72 cm, collar 39-40',
        ]);
    }
}
