<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $categories=Category::latest()->get();
        return view('backend.categories.index',[
        // dd($categories);
        'categories'=> $categories
        ]);

    }

    public function create(){
        return view('backend.categories.create');
    }
    public function store(CategoryRequest $request){

        try{
        //     dd(request() -> all());
        // request()->validate([
        //     'title'=>'required|min:3|max:10|unique:categories,title',
        //     'description'=>['required','min:10'], 
        //     // 'title'=>['required','min:3','max:10'],
        //     // 'description'=>['required','min:10', Rule::unique('categories','description')],
        // ]);
       
        Category::create([
            'title'=>$request->title,
            'description'=>$request->description,
        ]);
        // $request->session()->flash->('message','Task was Successful!');
        return redirect()->route('categories.index')->withMessage('Successfully Created!');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage);
        }
    
    }
    public function show(Category $category){
        // $category=Category::findOrFail($id);
        // dd($id);
        // dd($category);
        return view('backend.categories.show',[
            'category'=>$category
        ]);
    }
    public function edit(Category $category){
        
        return view('backend.categories.edit',[
            'category'=>$category
        ]);
    }
    public function update(Request $request, Category $category){
        try{
            // dd(request() -> all());
        request()->validate([
            'title'=>'required|min:3|max:10|unique:categories,title,'. $category->id,
            // 'title'=>['required','min:3','max:10'],
        //     'description'=>['required','min:10', Rule::unique('categories','description')
        // ->ignore($category->id)],
        'description'=>['required','min:10'],
        ]);
        $category->update([
            'title'=>$request->title,
            'description'=>$request->description,
        ]);
        // $request->session()->flash->('message','Task was Successful!');
        return redirect()->route('categories.index')->withMessage('Successfully Updated!');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage);
        }

    }
    public function destroy(Category $category){
        try{
            $category->delete();
            return redirect()->route('categories.index')->withMessage('Successfully Deleted!');
        }
        catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
    }
}
