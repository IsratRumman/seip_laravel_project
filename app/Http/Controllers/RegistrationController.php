<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Models\Registration;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function index(){
        $registrations=Registration::latest()->get();
        return view('backend.registrations.index',[
        
        'registrations'=> $registrations
        ]);

    }

    public function create(){
        return view('backend.registrations.create');
    }
    public function store(RegistrationRequest $request){
        try{
            
            Registration::create([
            'firstname'=>$request->firstname,
            'lastname'=>$request->firstname,
            'email'=>$request->email,
            'password'=>$request->password,
            'presentAddress'=>$request->presentAddress,
            'permanentAddress'=>$request->permanentAddress,
            'city'=>$request->city,
            'state'=>$request->state,
            'zipcode'=>$request->zipcode,
            'gender'=>$request->gender,
        ]);
        
        return redirect()->route('registrations.index')->withMessage('Successfully Created!');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            
        }
    
    }
    public function show(Registration $registration){
        
        return view('backend.registrations.show',[
            'registration'=>$registration
        ]);
    }
    public function edit(Registration $registration){
        
        return view('backend.registrations.edit',[
            'registration'=>$registration
        ]);
    }
    public function update(RegistrationRequest $request, Registration $registration){
        try{
        
        $registration->update([
            'firstname'=>$request->firstname,
            'lastname'=>$request->firstname,
            'email'=>$request->email,
            'password'=>$request->password,
            'presentAddress'=>$request->presentAddress,
            'permanentAddress'=>$request->permanentAddress,
            'city'=>$request->city,
            'state'=>$request->state,
            'zipcode'=>$request->zipcode,
            'gender'=>$request->gender,
        ]);

        return redirect()->route('registrations.index')->withMessage('Successfully Updated!');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
          
        }

    }
    public function destroy(Registration $registration){
        try{
            $registration->delete();
            return redirect()->route('registrations.index')->withMessage('Successfully Deleted!');
        }
        catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
    }
}
