<?php

namespace App\Http\Controllers;

use App\Http\Requests\SizeRequest;
use App\Models\Size;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index(){
        $sizes=Size::latest()->get();
        return view('backend.sizes.index',[
        
        'sizes'=> $sizes
        ]);

    }

    public function create(){
        return view('backend.sizes.create');
    }
    public function store(SizeRequest $request){

        try{
        //     dd(request() -> all());
        // request()->validate([
        //     'title'=>'required|min:3|max:10|unique:categories,title',
        //     'description'=>['required','min:10'], 
        //     // 'title'=>['required','min:3','max:10'],
        //     // 'description'=>['required','min:10', Rule::unique('categories','description')],
        // ]);
       
        Size::create([
            'title'=>$request->title,
            'description'=>$request->description,
        ]);
        // $request->session()->flash->('message','Task was Successful!');
        return redirect()->route('sizes.index')->withMessage('Successfully Created!');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage);
        }
    
    }
    public function show(Size $size){
        // $category=Category::findOrFail($id);
        // dd($id);
        // dd($category);
        return view('backend.sizes.show',[
            'size'=>$size
        ]);
    }
    public function edit(Size $size){
        
        return view('backend.sizes.edit',[
            'size'=>$size
        ]);
    }
    public function update(SizeRequest $request, Size $size){
        try{
        //     // dd(request() -> all());
        // request()->validate([
        //     'title'=>'required|min:3|max:10|unique:sizes,title,'. $size->id,
        //     // 'title'=>['required','min:3','max:10'],
        // //     'description'=>['required','min:10', Rule::unique('categories','description')
        // // ->ignore($category->id)],
        // 'description'=>['required','min:10'],
        // ]);
        // dd(request()->all());
        $size->update([
            'title'=>$request->title,
            'description'=>$request->description,
        ]);
        // $request->session()->flash->('message','Task was Successful!');
        return redirect()->route('sizes.index')->withMessage('Successfully Updated!');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage);
        }

    }
    public function destroy(Size $size){
        try{
            $size->delete();
            return redirect()->route('sizes.index')->withMessage('Successfully Deleted!');
        }
        catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
    }
}
