<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SizeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()

    {
        $sizeId=$this->route('size')->id ?? '';
        return [
            'title'=>'required|min:3|max:20|unique:sizes,title,'.$sizeId,
            // 'title'=>['required','min:3','max:10'],
            // 'description'=>['required','min:10', Rule::unique('categories','description'),]
            'description'=>['required','min:10'],
        ];
    }
}
