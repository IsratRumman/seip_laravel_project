<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        $registrationid =$this->route('registration')->id ?? '';
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required|min:6',
            'email' => 'required|unique:registrations,email,'.$registrationid,
            'presentAddress' => 'required',
            'permanentAddress' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'gender' => 'required',
            'remember' => 'required'
        ];
    }
}
